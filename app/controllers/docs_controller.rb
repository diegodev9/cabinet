class DocsController < ApplicationController
  before_action :set_doc, only: [:show, :edit, :update, :destroy]
  before_action :check_user, except: [:index, :new]

  def index
    @docs = Doc.where(user_id: current_user).order(created_at: :desc)
  end

  def show
  end

  def new
    @doc = Doc.new
  end

  def create
    @doc = Doc.new(doc_params)
    @doc.user_id = current_user.id

    if @doc.save
      redirect_to @doc, notice: 'New doc created'
    else
      render 'docs/new', notice: 'There was a problem'
    end
  end

  def edit
  end

  def update
    if @doc.update(doc_params)
      redirect_to @doc, notice: 'Doc updated'
    else
      render 'edit'
    end
  end

  def destroy
    @doc.destroy
    redirect_to docs_path
  end

  private
  def check_user
    redirect_to root_path, notice: "Forbidden action" unless @doc.user_id == current_user.id
  end

  def set_doc
    @doc = Doc.find(params[:id])
  end

  def doc_params
    params.require(:doc).permit(:title, :content, :user_id)
  end
end
