class ModifyUserIdOnDocs < ActiveRecord::Migration[6.1]
  def change
    remove_column :docs, :user_id
    add_reference :docs, :user, foreign_key: true
  end
end
